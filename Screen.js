import Animated, {
  useSharedValue,
  withTiming,
  useAnimatedStyle,
  Easing,
  withSpring,
  Layout,
  LightSpeedInLeft,
  AnimatedLayout,
  SlideInRight,
  useAnimatedGestureHandler,
} from 'react-native-reanimated';
import {
  View,
  Button,
  Touchable,
  TouchableOpacity,
  FlatList,
  Text,
  TextInput,
} from 'react-native';
import React, {useState} from 'react';
import {PanGestureHandler} from 'react-native-gesture-handler';

export default function AnimatedStyleUpdateExample(props) {
  const [data, setData] = useState([]);
  const translateX = useSharedValue(0);
  const translateY = useSharedValue(0);
  const pangesturehandle = useAnimatedGestureHandler({
    onStart: (event, context) => {
      context.a = translateX.value;
      context.b = translateY.value;
    },
    onActive: (event, context) => {
      translateX.value = event.translationX + context.a;
      translateY.value = event.translationY + context.b;
    },
  });
  const style = useAnimatedStyle(() => {
    return {
      transform: [
        {translateX: translateX.value},
        {translateY: translateY.value},
        {scale: withTiming(translateX.value < 100 ? 1 : 2)},
      ],
    };
  });
  return (
    <AnimatedLayout style={{flex: 1, flexDirection: 'column'}}>
      <PanGestureHandler onGestureEvent={pangesturehandle}>
        <Animated.View
          style={[{height: 100, width: 100, backgroundColor: 'black'}, style]}
        />
      </PanGestureHandler>
      {/*<FlatList
        data={data}
        renderItem={({item}) => {
          return (
            <Animated.View
              entering={SlideInRight}
              style={{justifyContent: 'space-between', flexDirection: 'row'}}>
              <Text>{item}</Text>
              <Text>add more</Text>
            </Animated.View>
          );
        }}
      />
      {data.map((data) => {
        return (
          <Animated.View
            entering={SlideInRight}
            layout={Layout.springify()}
            style={{justifyContent: 'space-between', flexDirection: 'row'}}>
            <Text>{data}</Text>
            <Text>add more</Text>
          </Animated.View>
        );
      })}
      <Button
        title="add"
        onPress={() => {
          setData(['Objective', ...data]);
        }}
      />*/}
    </AnimatedLayout>
  );
}
